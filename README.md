# Jira Custom Field Tutorial

This tutorial shows you how to create a new custom field type for Jira using the app development platform. 
You'll create a custom field that can only be edited as an admin user. 
Your custom field will be written and installed as a Jira app, and include a Java class that extends the 
GenericTextCFType class to enable storage and retrieval of your custom field values. 
You'll use a Velocity template, a format that combines Java and HTML, to render your field and control who 
can edit or view the field. To tether your Java class and template together, you'll define both in a single 
customfield-type plugin module in your atlassian-plugin.xml descriptor file. 

This tutorial covers the following topics: 

* An overview of custom fields and the files that comprise the app
* Extending the custom field type class, GenericTextCFType, for your app
* Using the customfield-type plugin module type for Jira

You can view the full tutorial here: [Creating a Custom Field Type][1]. 

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/server/jira/platform/creating-a-custom-field-type/