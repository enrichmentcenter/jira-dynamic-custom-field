require([
    'aui/flag',
    'jira/util/events',
    'jira/util/events/types',
    'jira/util/events/reasons',
    'jquery'
], function(flag, Events,Types,Reasons,$) {
    var eventsICareAbout = [
        Reasons.contentRefreshed,
        Reasons.dialogReady,
        Reasons.inlineEditStarted,
        Reasons.pageLoad
    ];

    Events.bind(Types.NEW_CONTENT_ADDED, function(e, el, reason) {
        if (eventsICareAbout.indexOf(reason) > -1) {
            $('.my-custom-toggle', el).each(function() {
                var thing = this.id || this.name;
                flag({title: reason, body: 'oh my! I found ' + thing });
            });
        }
    });
});
